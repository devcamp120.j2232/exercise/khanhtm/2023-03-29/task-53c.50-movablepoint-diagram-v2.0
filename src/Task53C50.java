import model.MovableCycle;
import model.MovablePoint;

public class Task53C50 {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(2, 3, 5, 1);
        MovablePoint center = new MovablePoint(1, 1, 2, 2);
        MovableCycle circle = new MovableCycle(10, center);
        System.out.println("Original point: " + point);
        System.out.println("Original cycle: " + circle);
        point.moveUp();
        point.moveRight();
        circle.moveDown();
        circle.moveLeft();
        System.out.println("New point: " + point);
        System.out.println("New cycle: " + circle);
    }
}
