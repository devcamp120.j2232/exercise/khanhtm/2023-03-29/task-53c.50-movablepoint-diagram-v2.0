package model;
import interfaces.Movable;
public class MovableCycle implements Movable  {
    private int radius;
    private MovablePoint center;
    public MovableCycle(int radius, MovablePoint center) {
        this.radius = radius;
        this.center = center;
    }
    public int getRadius() {
        return radius;
    }
    public void setRadius(int radius) {
        this.radius = radius;
    }
    public MovablePoint getCenter() {
        return center;
    }
    public void setCenter(MovablePoint center) {
        this.center = center;
    }
    @Override
    public String toString() {
        return center.toString() + ", radius=" + radius;
    }
    public void moveUp(){
        center.moveUp();
    }
    public void moveDown(){
        center.moveDown();;
    }
    public void moveLeft(){
        center.moveLeft();
    }
    public void moveRight(){
        center.moveRight();
    }
}
